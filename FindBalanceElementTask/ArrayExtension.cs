﻿using System;

namespace FindBalanceElementTask
{
    public static class ArrayExtension
    {
        public static int? FindBalanceElement(int[] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (array.Length == 0)
            {
                throw new ArgumentException("Array is empty.");
            }

            for (int i = 1; i < array.Length - 1; i++)
            {
                long leftSumArray = 0;
                long rightSumArray = 0;
                for (int l = 0; l < i; l++)
                {
                    leftSumArray += array[l];
                }

                for (int r = i + 1; r < array.Length; r++)
                {
                    rightSumArray += array[r];
                }

                if (leftSumArray == rightSumArray)
                {
                    return i;
                }
            }

            return null;
        }
    }
}
